﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.Json;
using System.IO.Compression;
using Newtonsoft.Json;

namespace cf2installer
{
    public partial class Form1 : Form
    {
        private string _sourceModsPath;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //Steam info from windows registry
            _sourceModsPath = (string)Registry.GetValue("HKEY_CURRENT_USER\\Software\\Valve\\Steam", "SourceModInstallPath", "") + "\\";
            string steamExePath = (string)Registry.GetValue("HKEY_CURRENT_USER\\Software\\Valve\\Steam", "SteamExe", "");
            string modInstallFolder = _sourceModsPath + "cf2";

            //No sourcemods folder - steam may be broken...
            if (string.IsNullOrEmpty(_sourceModsPath))
            {
                lblStatus.Text = "No sourcemods folder detected!";
                return;
            }

            //No steam exe - this probably means steam is not installed
            if (string.IsNullOrEmpty(steamExePath))
            {
                lblStatus.Text = "No Steam installation found!";
                return;
            }

            //Check if the mod already exists by looking for its gameinfo file
            if (File.Exists(modInstallFolder + "\\gameinfo.txt"))
            {
                btnUpdate.Enabled = true;
                btnUninstall.Enabled = true;
                lblStatus.Text = "Classic Fortress 2 installed in " + _sourceModsPath;
            }
            else
            {
                btnInstall.Enabled = true;
                btnUpdate.Enabled = true;
                lblStatus.Text = "Classic Fortress 2 is not installed.";
            }
        }

        private void btnInstall_Click(object sender, EventArgs e)
        {
            if (!Directory.Exists(_sourceModsPath))
            {
                Directory.CreateDirectory(_sourceModsPath);
            }

            // Get current version download path
            //Protocol and TLS stuff
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                   | SecurityProtocolType.Tls11
                   | SecurityProtocolType.Tls12;

            string sJson;
            WebClient wcJson = new WebClient();
            using (MemoryStream stream = new MemoryStream(wcJson.DownloadData("https://chrisfriend2.github.io/versions/")))
            {
                StreamReader reader = new StreamReader(stream);
                sJson = reader.ReadToEnd();
            }

            //Convert JSON file to a data table
            DataSet ds = JsonConvert.DeserializeObject<DataSet>(sJson);
            DataTable dtVersions = ds.Tables["versions"];

            string dlPath = (string)dtVersions.Rows[0]["dlUrl"];

            // Download it
            try
            {
                using (WebClient wc = new WebClient())
                {
                    wc.DownloadProgressChanged += wc_DownloadProgressChanged;
                    wc.DownloadFileCompleted += wc_DownloadFinished;
                    wc.DownloadFileAsync(new System.Uri(dlPath), _sourceModsPath + "cf2.zip");
                }

                btnInstall.Enabled = false;
                btnUninstall.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("There was an error starting the download.");
            }
        }

        // Event to track the progress
        void wc_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            //progressBar.Value = e.ProgressPercentage;
            lblStatus.Text = "Downloading: " + e.ProgressPercentage.ToString() + "%";
        }

        void wc_DownloadFinished(object sender, AsyncCompletedEventArgs e)
        {
            //Extract the files to the sourcemods folder when done
            try
            {
                MessageBox.Show(e.Error.ToString());
                return;
            }
            catch
            {
            }
            UnpackFiles();
            CleanupFiles("cf2.zip");
            MessageBox.Show("Classic Fortress 2 successfully installed. Relaunch Steam to make it appear in the library.");
        }

        void UnpackFiles()
        {
            ZipFile.ExtractToDirectory(_sourceModsPath + "cf2.zip", _sourceModsPath);
        }

        void CleanupFiles(string sFile)
        {
            File.Delete(_sourceModsPath + sFile);
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            //Protocol and TLS stuff
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                   | SecurityProtocolType.Tls11
                   | SecurityProtocolType.Tls12;

            // Get current local mod version
            string sVersion = "";
            try
            {
                sVersion = File.ReadAllText(_sourceModsPath + "cf2\\version.txt");
            }
            catch
            {
                MessageBox.Show("Could not find version number.");
            }
            int iVersion = 0;
            if (int.TryParse(sVersion, out iVersion))
            {
                //Read JSON file from server
                string sJson;
                WebClient wc = new WebClient();
                try
                {
                    using (MemoryStream stream = new MemoryStream(wc.DownloadData("https://chrisfriend2.github.io/versions/")))
                    {
                        StreamReader reader = new StreamReader(stream);
                        sJson = reader.ReadToEnd();
                    }

                    //Convert JSON file to a data table
                    DataSet ds = JsonConvert.DeserializeObject<DataSet>(sJson);
                    DataTable dtVersions = ds.Tables["versions"];

                    //Make a list of versions to download (if any) to download later
                    List<string> ListUrlsToGet = new List<string>();
                    foreach (DataRow row in dtVersions.Rows)
                    {
                        int rowVersion = int.Parse((string)row["versionNumber"]);
                        if (rowVersion > iVersion)
                        {
                            ListUrlsToGet.Add((string)row["dlUrl"]);
                        }
                    }

                    //Now download the update files
                    foreach (string sUrl in ListUrlsToGet)
                    {
                        using (WebClient wcUpdate = new WebClient())
                        {
                            wcUpdate.DownloadProgressChanged += wc_DownloadProgressChanged;
                            wcUpdate.DownloadFileCompleted += wc_DownloadUpdateFinished;
                            wcUpdate.DownloadFileAsync(new System.Uri(sUrl), _sourceModsPath + "cf2.zip");
                        }
                    }

                    if (ListUrlsToGet.Count == 0)
                    {
                        lblStatus.Text = "Game is up to date.";
                    }
                    else
                    {

                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error getting update. Check your internet connection. " + ex.Message);
                }
            }
        }

        void wc_DownloadUpdateFinished(object sender, AsyncCompletedEventArgs e)
        {
            //Extract the files to the sourcemods folder when done
            try
            {
                MessageBox.Show(e.Error.ToString());
                return;
            }
            catch
            {
            }
            UnpackUpdateFiles();
            CleanupFiles("cf2.zip");

            // Label stuff here because async is weird
            lblStatus.Text = "Update successfully installed.";
        }

        void UnpackUpdateFiles()
        {
            // Unzip it
            using (ZipArchive archive = ZipFile.OpenRead(_sourceModsPath + "cf2.zip"))
            {
                foreach (ZipArchiveEntry entry in archive.Entries)
                {
                    string sPath = Path.Combine(_sourceModsPath, entry.FullName);

                    if (String.IsNullOrEmpty(entry.Name))
                    {
                        // We don't need to check if the directory already exists because CreateDirectory won't do anything to existing directories
                        Directory.CreateDirectory(sPath);
                    }
                    else
                    {
                        entry.ExtractToFile(sPath, true);
                    }
                }
            }

            // Check if theres any old files that need to be deleted from the game install
            try
            {
                using (StreamReader reader = new StreamReader(_sourceModsPath + "\\cf2\\deletefiles.json"))
                {
                    string jsonDelete = reader.ReadToEnd();

                    DataSet ds = JsonConvert.DeserializeObject<DataSet>(jsonDelete);
                    DataTable dtFiles = ds.Tables["files"];

                    foreach(DataRow row in dtFiles.Rows)
                    {
                        File.Delete(_sourceModsPath + "\\cf2\\" + (string)row[0]);
                    }
                }
            }
            catch { }


        }

        private void btnUninstall_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure you want to uninstall? This will delete all configuration files, downloaded content, and custom content in Classic Fortress 2.", "Uninstall Classic Fortress 2", MessageBoxButtons.YesNo);

            if (result == DialogResult.Yes)
            {
                Directory.Delete(_sourceModsPath + "cf2", true);

                lblStatus.Text = "Classic Fortress 2 successfully uninstalled.";
                btnInstall.Enabled = true;
                btnUpdate.Enabled = false;
                btnUninstall.Enabled = false;
            }
        }
    }
}
